package com.devcamp.j02_javabasic.s10;

public class WrapperExample {
    public static void main(String[] args) throws Exception {
        //WrapperExample.autoBoxing();
        WrapperExample.unBoxing();
    }

    public static void autoBoxing() {
        byte myByte = 1 ; // 1 byte whole num -128 to 127
        short myShortNum = 11; // 2 bytes whole num -32,768 to 32,767
        int myIntNum = 111; // 4 bytes integer (whole number)
        long myLongNum = 1111; // 8 bytes
        float myFloatNum = 11.11f; // 4 bytes 6 to 7 decimal digits (floating point number)
        double myDoubleNum = 111.111d; // 8 bytes 15 decimal digits
        boolean myBool = true; // 1 bit boolean
        char myLetter = 'A'; // 2 bytes character

        Byte myByteObj = myByte; 
        Short myShortNumObj = myShortNum; 
        Integer myIntNumObj = myIntNum; 
        Long myLongNumObj = myLongNum; 
        Float myFloatNumObj = myFloatNum; 
        Double myDoubleNumObj = myDoubleNum; 
        Boolean myBoolObj = myBool; 
        Character myLetterObj = myLetter; 

        System.out.println("Autoboxing: chuyển primitive data thành wrapper object");
        System.out.println("myByteObj = " + myByteObj);
        System.out.println("myShortNumObj = " + myShortNumObj);
        System.out.println("myIntNumObj = " + myIntNumObj);
        System.out.println("myLongNumObj = " + myLongNumObj);
        System.out.println("myFloatNumObj = " + myFloatNumObj);
        System.out.println("myDoubleNumObj = " + myDoubleNumObj);
        System.out.println("myBoolObj = " + myBoolObj);
        System.out.println("myLetterObj = " + myLetterObj);
    }

    public static void unBoxing() {
        Byte myByteObj = 2; 
        Short myShortNumObj = 22; 
        Integer myIntNumObj = 222; 
        Long myLongNumObj = (long) 2222; 
        Float myFloatNumObj = 22.22f; 
        Double myDoubleNumObj = 222.222d; 
        Boolean myBoolObj = false; 
        Character myLetterObj = 'B';
        
        byte myByteValue = myByteObj; 
        short myShortNumValue = myShortNumObj; 
        int myIntNumValue = myIntNumObj; 
        long myLongNumValue = myLongNumObj; 
        float myFloatNumValue = myFloatNumObj; 
        double myDoubleNumValue = myDoubleNumObj; 
        boolean myBoolValue = myBoolObj; 
        char myLetterValue = myLetterObj;

        System.out.println("Unboxing: chuyển wrapper object thành primitive data");
        System.out.println("myByteValue = " + myByteValue);
        System.out.println("myShortNumValue = " + myShortNumValue);
        System.out.println("myIntNumValue = " + myIntNumValue);
        System.out.println("myLongNumValue = " + myLongNumValue);
        System.out.println("myFloatNumValue = " + myFloatNumValue);
        System.out.println("myDoubleNumValue = " + myDoubleNumValue);
        System.out.println("myBoolValue = " + myBoolValue);
        System.out.println("myLetterValue = " + myLetterValue);
    }
}
